package day_6

import measureMedians
import java.io.File

fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun loadData(): List<Int> {
    val txtLines = File("data/day_6.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.single().let { line -> line.trim().split(",").map { it.toInt() } }
}

fun stepOne(): Int {
    val initialPopulation = loadData()
    var pop = initialPopulation;
    for (day in 1..80) {
        val numBabies = pop.count { it == 0 };
        pop = pop.map { if (it == 0) 6 else it - 1 }.plus(List(numBabies) { 8 }.toList())
    }
    return pop.size
}

fun stepTwo(): Long {
    val initialPopulation = loadData()
    val t = mutableListOf<Long>();
    t.fill(0)
    val counts = initialPopulation
        .fold(MutableList(9) { 0L }) { acc, i ->
            acc[i] = acc[i] + 1;
            acc
        }.toList()
        .let { liveCounts ->
            (1..256).fold(liveCounts) { acc, _ ->
                val t = acc.rotateLeft(1)
                val y = t.update(6) { t[6] + t[8] }
                y
            }
        };
    return counts.sum()
}

fun <T> Collection<T>.rotateLeft(n: Int) = this.drop(n) + this.take(n)
fun <T> List<T>.update(index: Int, computeValue: () -> T) = this.toMutableList().also { it[index] = computeValue() }
