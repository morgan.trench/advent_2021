package day_12

import measureMedians
import java.io.File
import java.util.*
import javax.print.attribute.standard.Destination
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

private fun loadData(): Map<String, List<String>> {
    val txtLines = File("data/day_12.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.map { line -> line.trim().split("-").let { it[0] to it[1] } }
        .fold(mutableMapOf<String, List<String>>()){ acc, pair ->
            acc[pair.first] = (acc.getOrDefault(pair.first, emptyList()) + pair.second)
            acc[pair.second] = (acc.getOrDefault(pair.second, emptyList()) + pair.first)
            acc
        }
}

private fun possiblePaths(connections: Map<String, List<String>>, isValidDestination: (path: List<String>, destination: String) -> Boolean, startNode: String = "start", endNode: String = "end"): List<List<String>> {
    val paths = mutableListOf<List<String>>()
    val queue = (connections[startNode]?: throw Exception("Unable to find connections for node \"$startNode\"")).map { listOf("start", it) }.toMutableList()
    while(queue.isNotEmpty()){
        val path = queue.removeLast()
        val node = path.last();
        if (node == endNode){
            paths.add(path)
        } else {
            val possibleDestinations = connections[node] ?: throw Exception("Unable to find connections for node \"$node\"")
            val validDestinations = possibleDestinations.filter { isValidDestination(path, it) }
            queue.addAll(validDestinations.map { edge ->  path + edge })
        }
    }
    return paths
}

private fun stepOne(): Int {
    val connections = loadData()
    val smallCaves = (connections.keys.toSet() - setOf("start", "end")).filter { it.lowercase(Locale.getDefault()) == it }
    val isValidDestination: (List<String>, String) -> Boolean =  { path, destination -> destination != "start" && (!smallCaves.contains(destination) || smallCaves.contains(destination) && !path.contains(destination)) }
    val paths = possiblePaths(connections, isValidDestination)
    return paths.size
}

private fun stepTwo(): Int {
    val connections = loadData()
    val smallCaves = (connections.keys.toSet() - setOf("start", "end")).filter { it.lowercase(Locale.getDefault()) == it }
    fun isValidDestination(path: List<String>, destination: String): Boolean {
        if (destination == "start") return false;
        val haveVisitedASmallCaveTwice = smallCaves.any { sc -> path.count { node -> sc == node } == 2 }
        return if (haveVisitedASmallCaveTwice) {
            (!smallCaves.contains(destination) || smallCaves.contains(destination) && !path.contains(destination))
        } else {
            true
        }
    }
//    val isValidDestination: (List<String>, String) -> Boolean =  { path, destination -> (destination != "start") && (!(smallCaves.any { sc -> path.count { node -> sc == node } == 2 }) || ((smallCaves.any { sc -> path.count { node -> sc == node } == 2 }) && (!smallCaves.contains(destination) || smallCaves.contains(destination) && !path.contains(destination)))) }
    val paths = possiblePaths(connections, ::isValidDestination)
    return paths.size
}
