package day_8

import measureMedians
import java.io.File

fun main(){
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun loadDataOne(): List<List<String>> {
    val txtLines = File("data/day_8.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.map{ line -> line.split(" | ")[1].trim()}.map { line -> line.split(" ") }
}

fun stepOne(): Int {
    val outputs = loadDataOne();
    val uniqueLengths = setOf<Int>(2, 3, 4, 7)
    return outputs.flatten().count { uniqueLengths.contains(it.length) };
}

fun loadDataTwo(): List<Pair<Set<Set<Char>>, List<Set<Char>>>> {
    val txtLines = File("data/day_8.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.map{ line -> line.split(" | ").let { it[0].trim().split(" ").map{ rep -> rep.toSet() }.toSet() to it[1].trim().split(" ").map { it.toSet() } } }
}

fun stepTwo(): Int {
    val data = loadDataTwo();
    val values = data.map { case ->
        val (enumeration, output) = case;
        val remaining = enumeration.toMutableSet()

        // Givens
        val one = remaining.findSingleAndRemove { it.size == 2 };
        val four = remaining.findSingleAndRemove { it.size == 4 };
        val seven = remaining.findSingleAndRemove { it.size == 3 };
        val eight = remaining.findSingleAndRemove { it.size == 7 };

        // Inferences
        val six = remaining.findSingleAndRemove { (it - seven).size == 4 }
        val five = remaining.findSingleAndRemove { (it - six).isEmpty() }
        val two = remaining.findSingleAndRemove { (it intersect  five).size == 3 }
        val nine = remaining.findSingleAndRemove { (it intersect five).size == 5 }
        val zero = remaining.findSingleAndRemove { it.size == 6 }
        val three = remaining.single()

        // Calculate Actual Value
        val repToDigit = listOf(zero, one, two, three, four, five, six, seven, eight, nine).mapIndexed{ index, rep -> rep to index }.toMap()
        output.map { repToDigit[it]!! }.joinToString("").toInt()
    }
    return values.sum();
}

private fun <T> MutableSet<T>.findSingleAndRemove(predicate: (T) -> Boolean): T = this.single(predicate).also { this.remove(it) }