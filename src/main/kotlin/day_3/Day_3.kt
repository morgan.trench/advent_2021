package day_3

import measureMedians
import java.io.File

fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun stepOne(): Int {
    File("data/day_3.txt").useLines { data ->
        var length = 0;
        val sums = data.map { line -> line.toCharArray().map { it.digitToInt() } }.reduce { acc, current ->
            length += 1
            acc.mapIndexed { index, total ->
                total + current[index]
            }
        }

        val threshold = length / 2.0
        val gammaRateBinaryStr = sums.map { if (it > threshold) 1 else 0 }.joinToString("")
        val gammaRate = gammaRateBinaryStr.toInt(2);
        val epsilonRate = gammaRate.inv().and((1 shl sums.size) - 1);
        val power = gammaRate * epsilonRate;
        return power
    }
}

fun stepTwo(): Int {
    val data = File("data/day_3.txt").readLines();

    var oxygenData = data.toList();
    var iteration = -1;
    do {
        iteration += 1;
        oxygenData = oxygenData.filterUsingBitAt(iteration)
    } while (oxygenData.size > 1)

    val oxygenDataBinaryStr = oxygenData.single()
    val oxygenRating = oxygenDataBinaryStr.toInt(2)

    var scrubberData = data.toList();
    iteration = -1;
    do {
        iteration += 1;
        scrubberData = scrubberData.filterUsingBitAt(iteration, true)
    } while (scrubberData.size > 1)

    val scrubberDataBinaryStr = scrubberData.single()
    val scrubberRating = scrubberDataBinaryStr.toInt(2)

    return oxygenRating * scrubberRating
}

fun List<String>.filterUsingBitAt(index: Int, useLeastCommon: Boolean = false): List<String> {
    val mostCommonValue = if (2 * this.sumOf { it[index].toString().toInt() } >= this.size) 1 else 0;
    val filterFunction =
        if (useLeastCommon) { line: String -> line[index].digitToInt() != mostCommonValue } else { line: String -> line[index].digitToInt() == mostCommonValue }
    return this.filter(filterFunction)
}