package day_14

import measureMedians
import java.io.File
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun loadData(): Pair<String, Map<String, String>> {
    val txtLines = File("data/day_14.txt").readLines().filter { it.isNotEmpty() }
    val (templateTxt, pairInsertionsTxt) = txtLines.partition { !it.contains("->") }
    val insertRules = pairInsertionsTxt.associate { txt -> txt.split(" -> ").zipWithNext().single() }
    return templateTxt.single() to insertRules
}

fun stepOne(): Int {
    val (template, rules) = loadData();
    val polymer = (1..10).fold(template) { acc, _ -> (acc.windowed(2).map { pair -> pair.first() + rules[pair]!! } + acc.last()).joinToString("") }
    val counts = polymer.groupingBy { it }.eachCount().toList().sortedBy { it.second }
    return counts.last().second - counts.first().second
}

fun stepTwo(): Long {
    val (template, rules) = loadData();
    val edgeElements = setOf(template.first(), template.last())

    val pairCounts = (1..40).fold(template.windowed(2).groupingBy { it }.eachCount().mapValues { it.value.toLong() }) { acc, _ ->
        acc.flatMap { (pair, count) -> listOf((pair.first() + rules[pair]!!) to count, (rules[pair]!! + pair.last() to count)) }
            .groupingBy { it.first }.aggregate { _, count, element, _ -> (count ?: 0) + element.second }
    }

    val elementCounts = pairCounts.flatMap { (pair, count) -> listOf(pair.first() to count, pair.last() to count) }
        .groupBy({ it.first }) { it.second }
        .mapValues { (element, counts) -> (if (element in edgeElements) counts.sum() + 1 else counts.sum()) / 2 }
        .toList().sortedBy { it.second }

    return elementCounts.last().second - elementCounts.first().second
}