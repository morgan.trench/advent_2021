package day_16

import measureMedians
import java.io.File
import java.lang.Exception
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalTime::class)
fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun loadData(): String {
    val txtLines = File("data/day_16.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.single().trim()
}

fun stepOne(): Int {
    val hexString = loadData();
    val binaryString = hexString.toList().joinToString("") { it.digitToInt(16).toString(2).padStart(4, '0') }
    val (packet, padding) = extractPacket(binaryString)
    return packet!!.sumVersions()
}

fun stepTwo(): Long {
    val hexString = loadData();
    val binaryString = hexString.toList().joinToString("") { it.digitToInt(16).toString(2).padStart(4, '0') }
    val (packet, padding) = extractPacket(binaryString)
    return packet!!.evaluate()
}

@OptIn(ExperimentalStdlibApi::class)
private fun extractPacket(bitString: String): Pair<Packet?, String> {
    if (bitString.length < 8) return null to bitString;
    val bits = bitString.toMutableList()
    val (version, typeID) = bits.pop(6).chunked(3).map { it.joinToString("").toInt(2) }
    return if (typeID == 4){ // Literal
        val numChunks = bits.chunked(5).indexOfFirst { chunk -> chunk.first() == '0' } + 1
        val valueStr = bits.pop(5 * numChunks)
        val value = valueStr.chunked(5).joinToString("") { it.takeLast(4).joinToString("") }.toLong(2)
        Packet(version, typeID, emptyList(), value) to bits.toList().joinToString("")
    } else { // Operator
        val lengthTypeId = bits.pop(1).single()
        if (lengthTypeId == '0'){ // next 15 bits = number total bits for next subpackets
            val numBits = bits.pop(15).joinToString("").toInt(2)
            var remainingString: String = bits.pop(numBits).joinToString("")
            val subPackets = buildList<Packet> {
                do {
                    val (packet, leftover) = extractPacket(remainingString)
                    if (packet != null) add(packet)
                    remainingString = leftover
                } while (remainingString.isNotEmpty())
            }
            Packet(version, typeID, subPackets.toList()) to bits.joinToString("")
        } else { // next 11 bit = number of subpackets
            val numSubPackets = bits.pop(11).joinToString("").toInt(2)
            var remainingString = bits.joinToString("")
            val subPackets = List(numSubPackets) { extractPacket(remainingString).let { (packet, leftover) -> remainingString = leftover; packet!!} }
            Packet(version, typeID, subPackets) to remainingString
        }
    }
}

data class Packet(val version: Int, val typeId: Int, val subPackets: List<Packet>, val value: Long? = null)

private fun Packet.sumVersions(): Int = this.version + this.subPackets.sumOf { it.sumVersions() }

private fun Packet.evaluate(): Long = when (typeId){
    0 -> subPackets.sumOf { it.evaluate() }
    1 -> subPackets.map { it.evaluate() }.reduce { acc, x -> acc * x}
    2 -> subPackets.minOf { it.evaluate() }
    3 -> subPackets.maxOf { it.evaluate() }
    4 -> value!!
    5 -> subPackets.map { it.evaluate() }.let { (first, second) -> if (first > second) 1 else 0 }
    6 -> subPackets.map { it.evaluate() }.let { (first, second) -> if (first < second) 1 else 0 }
    7 -> subPackets.map { it.evaluate() }.let { (first, second) -> if (first == second) 1 else 0 }
    else -> throw Exception("Encountered unexpected typeId: $typeId")
}

private fun <T> MutableList<T>.pop(n: Int): List<T> {
    return List<T>(n) { this.removeFirst() }
}