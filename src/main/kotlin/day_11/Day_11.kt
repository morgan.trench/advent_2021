package day_11

import measureMedians
import java.io.File
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

private fun loadData(): Array<IntArray> {
    val txtLines = File("data/day_11.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.map { line -> line.trim().split("").filter { it.isNotEmpty() }.map { it.toInt() }.toIntArray() }.toTypedArray()
}

class Octopuses(private val energyLevels: Array<IntArray>){
    private val height = energyLevels.size; private val width = energyLevels.first().size; val total = height * width;

    fun step(): Int{
        energyLevels.forEachIndexed { y, row -> row.forEachIndexed { x, level -> energyLevels[y][x] = level + 1} }
        val flashed = mutableSetOf<Pair<Int, Int>>()
        var toFlash = energyLevels.flatMapIndexed { y, row -> row.mapIndexed{ x, level -> if (level > 9) x to y else null} }.filterNotNull()
        while(toFlash.isNotEmpty()) {
            val neighbours = toFlash
                .flatMap { (x, y) -> listOf(x - 1 to y - 1, x to y - 1, x + 1 to y - 1, x - 1 to y, x+1 to y, x - 1 to y + 1, x to y + 1, x + 1 to y + 1) }
                .filterNot { (x, y) -> x < 0 || y < 0 || x >= width || y >= height }
            neighbours.forEach { (nx, ny) -> energyLevels[ny][nx] += 1 }
            flashed.addAll(toFlash)
            toFlash = energyLevels.flatMapIndexed { y, row -> row.mapIndexed{ x, level -> if (level > 9) x to y else null} }.filterNotNull().filter { !flashed.contains(it) }
        }
        flashed.forEach { (x, y) -> energyLevels[y][x] = 0  }
        return flashed.size
    }
}

private fun stepOne(): Int {
    val octopuses = Octopuses(loadData())
    return generateSequence { octopuses.step() }.take(100).sum()
}

private fun stepTwo(): Int {
    val octopuses = Octopuses(loadData())
    return (generateSequence { octopuses.step() }.withIndex().find { (_, flashes) -> flashes == octopuses.total }?.index ?: throw Exception("Unable to find step where all octopuses flash")) + 1
}
