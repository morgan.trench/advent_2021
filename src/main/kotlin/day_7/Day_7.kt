package day_7

import day_5.Coordinate
import measureMedians
import java.io.File
import kotlin.math.abs

fun main(){
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun loadData(): List<Int> {
    val txtLines = File("data/day_7.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.single().let { line -> line.trim().split(",").map { it.toInt() } }
}

fun stepOne(): Int {
    val initialPositions = loadData();
    val range = initialPositions.minOrNull()!!..initialPositions.maxOrNull()!!
    val costs = range.map{ alignment -> alignment to initialPositions.sumOf{ abs(alignment - it)} }
    val cheapest = costs.minOf { it.second }
    return cheapest
}

fun stepTwo(): Int {
    val initialPositions = loadData();
    val range = initialPositions.minOrNull()!!..initialPositions.maxOrNull()!!
    val costs = range.map{ alignment -> alignment to initialPositions.sumOf{ (1..abs(alignment - it)).sum() } }
    val cheapest = costs.minOf { it.second }
    return cheapest
}