package day_9

fun alternativeStepTwo(): Int {
    val grid = loadData();
    val lows = mutableSetOf<Pair<Int, Int>>()
    for ((y, row) in grid.withIndex()){
        for ((x, height) in row.withIndex()){
            val neighbourHeights = listOfNotNull(
                if ((y - 1) >= 0) grid[y - 1][x] else null,
                if ((y + 1) < grid.size) grid[y + 1][x] else null,
                if ((x - 1) >= 0) grid[y][x - 1] else null,
                if ((x + 1) < row.size) grid[y][x + 1] else null,
            );
            if (neighbourHeights.all { it > height }) {
                lows.add(x to y)
            }
        }
    }

    val basins = mutableSetOf<Set<Pair<Int, Int>>>()
    for (low in lows){
        val queue = mutableListOf(low);
        val seen = mutableSetOf<Pair<Int, Int>>()
        while(queue.isNotEmpty()){
            val point = queue.removeFirst();
            val (x, y) = point
            val height = grid[y][x];
            if (height != 9){
                val neighbours = listOfNotNull(
                    if ((y - 1) >= 0) x to y - 1 else null,
                    if ((y + 1) < grid.size) x to y + 1 else null,
                    if ((x - 1) >= 0) x - 1 to y else null,
                    if ((x + 1) < grid[0].size) x + 1 to y else null,
                ).filter { !seen.contains(it) }
                seen.add(point)
                queue.addAll(neighbours);
            }
        }
        basins.add(seen)
    }
    return basins.toList().map { it.size }.sorted().takeLast(3).reduce{ acc, size -> acc * size  }
}