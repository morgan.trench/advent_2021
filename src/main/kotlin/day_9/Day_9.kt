package day_9

import measureMedians
import java.io.File
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
fun main(){
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("Answer 2 computed differently: ${alternativeStepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo, ::alternativeStepTwo)).joinToString(" and ")} respectively")
}

fun loadData(): List<List<Int>> {
    val txtLines = File("data/day_9.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.map { it.trim().toCharArray().map { it.digitToInt() } };
}

fun stepOne(): Int {
    val grid = loadData();
    val lows = mutableMapOf<Pair<Int, Int>, Int>()
    for ((y, row) in grid.withIndex()){
        for ((x, height) in row.withIndex()){
            val neighbours = listOfNotNull(
                if ((y - 1) >= 0) grid[y - 1][x] else null,
                if ((y + 1) < grid.size) grid[y + 1][x] else null,
                if ((x - 1) >= 0) grid[y][x - 1] else null,
                if ((x + 1) < row.size) grid[y][x + 1] else null,
            );
            if (neighbours.all { it > height }) {
                lows[(x to y)] = height
            }
        }
    }
    return lows.values.sumOf { (it + 1) };
}

fun stepTwo(): Int {
    val grid = loadData();
    val basins = mutableListOf<Set<Pair<Int, Int>>>()
    for ((y, row) in grid.withIndex()){
        for ((x, height) in row.withIndex()){
            if (height == 9) continue
            print('\r');
            val points = listOfNotNull(
                x to y,
                if ((y - 1) >= 0) x to y - 1 else null,
                if ((y + 1) < grid.size) x to y + 1 else null,
                if ((x - 1) >= 0)  x - 1 to y else null,
                if ((x + 1) < row.size) x + 1 to y else null,
            ).filter { grid[it.second][it.first] != 9 }.toSet();

            val foundBasins = basins.filter { basin -> points.any { point -> basin.contains(point) } }.toSet()

            if (foundBasins.size > 1){ // merge
                basins.removeAll(foundBasins)
                val merged = foundBasins.reduce{ acc, value -> acc.toSet().union(value).toMutableSet() }
                basins.add(merged);
            } else if (foundBasins.size == 1){
                val foundBasin = foundBasins.single()
                basins.remove(foundBasin)
                basins.add(foundBasin.union(points))
            } else {
                basins.add(points.toMutableSet())
            }
        }
    }
    return basins.map { it.size }.sorted().takeLast(3).reduce { acc, next -> acc * next};
}