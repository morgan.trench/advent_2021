package day_10

internal object Syntax {
    val corresponding: Map<Char, Char>;
    val openers: Set<Char>;
    val closers: Set<Char>;

    init {
        val pairings = setOf('(' to ')', '[' to ']', '{' to '}', '<' to '>')
        corresponding = (pairings.toList() + pairings.toList().map { it.second to it.first }).toMap()
        openers = pairings.map { it.first }.toSet()
        closers = pairings.map { it.second }.toSet()
    }
}

internal data class LineAnalysis(val openStack: List<Char>, val firstIllegalToken: Char?)