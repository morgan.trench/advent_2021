package day_10

import measureMedians
import java.io.File
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalTime::class)
fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun stepOne(): Int {
    val tokenizedLines = loadData();
    val firstIllegalCharacters = analyseTokenizedLines(tokenizedLines).mapNotNull { it.firstIllegalToken }
    val scoreMappings = mapOf(')' to 3, ']' to 57, '}' to 1197, '>' to 25137)
    return firstIllegalCharacters.sumOf { scoreMappings[it]!! };
}

fun stepTwo(): Long {
    val tokenizedLines = loadData();
    val autoComplete = analyseTokenizedLines(tokenizedLines)
        .filter { it.firstIllegalToken == null }
        .map { it.openStack.map { token -> Syntax.corresponding[token]!! }.reversed() }
    val scoreMappings = mapOf(')' to 1, ']' to 2, '}' to 3, '>' to 4)
    val scores = autoComplete.map { complete -> complete.fold(0L) { acc, c -> acc * 5 + scoreMappings[c]!! } }
    return scores.sorted()[scores.size / 2];
}

private fun loadData(): List<List<Char>> {
    val txtLines = File("data/day_10.txt").readLines().filter { it.isNotEmpty() }
    return txtLines.map { line -> line.trim().toCharArray().toList() }
}

private fun analyseTokenizedLines(lines: List<List<Char>>) = lines.map { line ->
    line.fold<Char, Pair<MutableList<Char>, Char?>>(mutableListOf<Char>() to null) { acc, token ->
        acc.also { (openStack, _) ->
            when {
                Syntax.openers.contains(token) -> openStack.add(token);
                Syntax.closers.contains(token) -> if (openStack.removeLast() != Syntax.corresponding[token]) return@fold openStack to token
                else -> throw Exception("Unexpected token '$token' is neither an opener or closer")
            }
        }
    }.let { (openStack, firstIllegalCharacter) -> LineAnalysis(openStack, firstIllegalCharacter) }
}