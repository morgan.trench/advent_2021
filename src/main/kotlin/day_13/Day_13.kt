package day_13

import measureMedians
import java.io.File
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalTime::class)
fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

private fun loadData(): Pair<Set<Pair<Int, Int>>, List<Pair<String, Int>>> {
    val txtLines = File("data/day_13.txt").readLines().filter { it.isNotEmpty() }
    val (dotLines, foldLines) = txtLines.partition { !it.contains("fold along") }
    val dots = dotLines.map { line -> line.trim().split(",").let { it[0].toInt() to it[1].toInt() } }.toSet()
    val folds = foldLines.map { line -> line.removePrefix("fold along ").trim().split("=").let { it[0] to it[1].toInt() } }
    return dots to folds
}

private fun stepOne(): Int{
    val (dots, folds) = loadData();
    val (foldAxis, foldIndex ) = folds[0];

    val newDots = if (foldAxis == "x"){
        val (same, toFlip) = dots.partition { dot -> dot.first < foldIndex }
        val flipped = toFlip.map { dot -> 2*foldIndex - dot.first to dot.second }
        (same + flipped).toSet()
    } else {
        val (same, toFlip) = dots.partition { dot -> dot.second < foldIndex }
        val flipped = toFlip.map { dot -> dot.first to 2*foldIndex - dot.second }
        (same + flipped).toSet()
    }

    return newDots.size;
}

private fun stepTwo(): String{
    val (dots, folds) = loadData();

    val finalDots = folds.fold(dots) { acc, (foldAxis, foldIndex) ->
        val (shouldFold, applyFold) = when (foldAxis){
            "x" -> ({ dot: Pair<Int, Int> -> dot.first < foldIndex } to { dot: Pair<Int, Int> -> 2*foldIndex - dot.first to dot.second })
            "y" -> ({ dot: Pair<Int, Int> -> dot.second < foldIndex } to { dot: Pair<Int, Int> -> dot.first to 2*foldIndex - dot.second })
            else -> throw Exception("Unknown Action")
        }
        val (same, toFold) = acc.partition(shouldFold)
        val flipped = toFold.map(applyFold)
        (same + flipped).toSet()
    }

    val (fWidth, fHeight) = finalDots.maxOf { it.first } + 1 to finalDots.maxOf { it.second } + 1
    val visual = List(fHeight) { y -> List(fWidth) {x -> if ( finalDots.contains(x to y)) "#" else "."} }
    return visual.joinToString("\n", prefix = "\n") { it.joinToString("") }
}