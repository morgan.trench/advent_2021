package day_17

import measureMedians
import median
import java.io.File
import javax.smartcardio.CardTerminal
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

private fun loadData(): Pair<IntRange, IntRange> {
    val txtLines = File("data/day_17.txt").readLines().filter { it.isNotEmpty() }
    val line = txtLines.single().trim()
    val (xeq, yeq) = line.removePrefix("target area: ").split(", ")
    val xRange = xeq.removePrefix("x=").split("..").map { it.toInt() }.let { (min, max) -> min..max }
    val yRange = yeq.removePrefix("y=").split("..").map { it.toInt() }.let { (min, max) -> min..max }
    return xRange to yRange
}

private fun stepOne(): Int {
    val target = loadData()
    val xs = leastTriangleIndex(target.first.first)..target.first.last;
    val ys = (if (target.second.first > 0) leastTriangleIndex(target.second.first) else target.second.first )..(122) // TODO figure out a better way than a magic number
    val searchSpace = xs.asSequence().flatMap { x -> ys.asSequence().map { y -> x to y } }
    val onTarget = searchSpace.mapNotNull { vZero -> simulate(vZero, target)?.let { trajectory -> vZero to trajectory } }.toList()
    val sorted = onTarget.sortedBy { (vZero, traj) -> traj.maxOf { (x, y) -> y } }
    val winner = sorted.last()
    return winner.let { (vZero, traj) -> traj.maxOf { (x, y) -> y }  }
}

private fun stepTwo(): Int {
    val target = loadData()
    val xs = leastTriangleIndex(target.first.first)..target.first.last;
    val ys = (if (target.second.first > 0) leastTriangleIndex(target.second.first) else target.second.first )..(122) // TODO figure out a better way than a magic number
    val searchSpace = xs.asSequence().flatMap { x -> ys.asSequence().map { y -> x to y } }
    val onTarget = searchSpace.filter { vZero -> simulate(vZero, target) != null }.toSet()
    return onTarget.size
}

private fun simulate(initialVelocity: Pair<Int, Int>, target: Pair<IntRange, IntRange>, initialPosition: Pair<Int, Int> = 0 to 0): List<Pair<Int, Int>>? {
    val trajectory = mutableListOf(initialPosition)
    var (x, y) = initialPosition
    var (vx, vy) = initialVelocity
    while (!sequenceOf((vx == 0 && x !in target.first), (vy < 0 && y < target.second.first), (vx < 0 && x < target.first.first), (vx > 0 && x > target.first.last),).any { it }){
        x += vx; y += vy;
        val newPosition = x to y
        trajectory.add(newPosition)
        if (withinBox(newPosition, target)) return trajectory
        if (vx != 0){ vx = if (vx > 0) vx - 1 else vx + 1 }
        vy -= 1;
    }
    return null
}

private fun withinBox(position: Pair<Int, Int>, box: Pair<IntRange, IntRange>): Boolean {
    val (x, y) = position;
    val (xs, ys) = box;
    return (x in xs) && (y in ys)
}

private fun leastTriangleIndex(target: Int)= sequence { var index = 0; var total = 0; while (true) { total += index; index +=1; yield(total) } }.indexOfFirst { it > target }