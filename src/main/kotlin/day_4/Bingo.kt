package day_4

import kotlin.math.roundToInt
import kotlin.math.sqrt

class BingoCell(val value: Int){
    var seen: Boolean = false
}

class BingoBoard(grid: List<Int>){
    private val dimension = sqrt(grid.size.toFloat()).roundToInt()
    private val cells = grid.map { value -> BingoCell(value) }
    private val rows by lazy { cells.chunked(dimension) }
    private val columns by lazy { cells.withIndex().groupBy( { (index) -> index % dimension }, { it.value }).entries.toList().sortedBy { it.key }.map { it.value } }
    var completed = false
    var score: Int? = null

    fun update(number: Int) {
        if (completed) throw Exception("Cannot update a completed Bingo Board")
        val cell = cells.find { it.value == number }
        if (cell != null) {
            cell.seen = true
            completed = completed || rows.any { row -> row.all { cell -> cell.seen } } || columns.any { column -> column.all { cell -> cell.seen } }
            if (completed) score = number * cells.filter { !it.seen }.sumOf { it.value }
        }
    }
}