package day_4

import measureMedians
import java.io.File

fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun loadBingo(): Pair<List<Int>, List<BingoBoard>> {
    val lines = File("data/day_4.txt").readLines().filter { it.isNotEmpty() }
    val draws = lines.first().let { line -> line.splitToSequence(",").map { it.toInt() } }.toList()
    val boards = lines.subList(1, lines.size).map { line -> line.trim().split("\\s+".toRegex()).map { it.toInt() } }.chunked(5)
        .map { rows -> BingoBoard(rows.flatten()) }
    return draws to boards
}

fun stepOne(): Int? {
    val (numbers, boards) = loadBingo()
    val queue = numbers.toMutableList();

    val completed: MutableList<BingoBoard> = mutableListOf()
    var remaining: List<BingoBoard> = boards.toList()
    var number: Int
    do {
        number = queue.removeFirst()
        remaining.forEach { it.update(number) }
        val (justCompleted, stillPlaying) = remaining.partition { it.completed }
        completed.addAll(justCompleted); remaining = stillPlaying;
    } while (completed.isEmpty())

    val winner = completed.single()
    return winner.score

}

fun stepTwo(): Int? {
    val (numbers, boards) = loadBingo()
    val queue = numbers.toMutableList();

    val completed: MutableList<BingoBoard> = mutableListOf()
    var remaining: List<BingoBoard> = boards.toList()
    var number: Int
    do {
        number = queue.removeFirst()
        remaining.forEach { it.update(number) }
        val (justCompleted, stillPlaying) = remaining.partition { it.completed }
        completed.addAll(justCompleted); remaining = stillPlaying;
    } while (remaining.isNotEmpty())

    val loser = completed.last()
    return loser.score
}

