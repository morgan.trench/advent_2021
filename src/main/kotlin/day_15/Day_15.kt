package day_15

import measureMedians
import java.io.File
import java.util.*
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun loadData(): Pair<List<List<Int>>, Pair<Int, Int>> {
    val txtLines = File("data/day_15.txt").readLines().filter { it.isNotEmpty() }
    val riskLevels =  txtLines.map { line -> line.toCharArray().map { char -> char.digitToInt() } }
    val dimensions = riskLevels.first().size to riskLevels.size
    return riskLevels to dimensions
}

fun stepOne(): Int {
    val (riskLevels, dimensions) = loadData()
    val (width, height) = dimensions;
    val pathQueue = PriorityQueue<Pair<Int, List<Pair<Int, Int>>>>() { a, b -> a.first - b.first}
    pathQueue.add(0 to listOf(0 to 0))
    val visited = mutableSetOf<Pair<Int, Int>>()
    while (pathQueue.isNotEmpty() && pathQueue.peek().second.last() != (width - 1 to height - 1)){
        val (cost, path) = pathQueue.remove()
        val point = path.last();
        val (x, y) = point
        if (point in visited) continue;
        visited.add(x to y)
        val nexts = listOfNotNull(
            if (x - 1 > -1) x - 1 to y else null,
            if (x + 1 < width) x + 1 to y else null,
            if (y - 1 > -1) x to y - 1 else null,
            if (y + 1 < height) x to y + 1 else null,
        ).filter { it !in visited }
        val newPaths = nexts.map { (nx, ny) -> cost + riskLevels[ny][nx] to path + (nx to ny) }
        pathQueue.addAll(newPaths)
    }
    val lowestRiskPath = pathQueue.remove();
    return lowestRiskPath.first;
}

fun stepTwo(): Int {
    val (baseRiskLevels, baseDimensions) = loadData()
    val (width, height) = baseDimensions.first * 5 to baseDimensions.second * 5;
    val riskLevels = List(height) { y -> List(width) { x ->
        val by = y % baseDimensions.second;
        val bx = x % baseDimensions.first;
        val modifier = x / baseDimensions.first + y/baseDimensions.second;
        val intermediate = (baseRiskLevels[by][bx] + modifier)
        if (intermediate < 10) intermediate else intermediate - 9
    } }

    val pathQueue = PriorityQueue<Pair<Int, List<Pair<Int, Int>>>>() { a, b -> a.first - b.first}
    pathQueue.add(0 to listOf(0 to 0))
    val visited = mutableSetOf<Pair<Int, Int>>()
    while (pathQueue.isNotEmpty() && pathQueue.peek().second.last() != (width - 1 to height - 1)){
        val (cost, path) = pathQueue.remove()
        val point = path.last();
        val (x, y) = point
        if (point in visited) continue;
        visited.add(x to y)
        val nexts = listOfNotNull(
            if (x - 1 > -1) x - 1 to y else null,
            if (x + 1 < width) x + 1 to y else null,
            if (y - 1 > -1) x to y - 1 else null,
            if (y + 1 < height) x to y + 1 else null,
        ).filter { it !in visited }
        val newPaths = nexts.map { (nx, ny) -> cost + riskLevels[ny][nx] to path + (nx to ny) }
        pathQueue.addAll(newPaths)
    }
    
    val lowestRiskPath = pathQueue.remove();
    return lowestRiskPath.first;
}