package day_5

import measureMedians
import java.io.File
import kotlin.math.max
import kotlin.math.min

data class Coordinate(val x: Int, val y: Int)

fun main(){
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun loadData(): List<Pair<Coordinate, Coordinate>> {
    val txtLines = File("data/day_5.txt").readLines().filter { it.isNotEmpty() }
    return txtLines
        .map { txt -> txt.split("->").map { it.trim() } }
        .map { txtSegment ->
            txtSegment.map { txtCoord -> txtCoord.split(",").map { it.toInt() }.let { Coordinate(it[0], it[1]) } }
        }.map { it.let { it[0] to it[1] } };
}

fun stepOne(): Int {
    val segments = loadData()
    val world = mutableMapOf<Pair<Int, Int>, Int>()
    for ((start, stop) in segments) {
        val (startX, startY) = start
        val (endX, endY) = stop
        if (startX == endX) {
            for (y in min(startY, endY)..max(startY, endY)) {
                val coord = startX to y;
                world[coord] = world.getOrDefault(coord, 0) + 1
            }
        } else if (startY == endY) {
            for (x in min(startX, endX)..max(startX, endX)) {
                val coord = x to startY;
                world[coord] = world.getOrDefault(coord, 0) + 1
            }
        }
    }
    return world.values.count { it > 1 }
}

fun stepTwo(): Int {
    val segments = loadData()
    val world = mutableMapOf<Pair<Int, Int>, Int>()
    for ((start, stop) in segments) {
        val (startX, startY) = start
        val (endX, endY) = stop
        if (startX == endX) {
            for (y in min(startY, endY)..max(startY, endY)) {
                val coord = startX to y;
                world[coord] = world.getOrDefault(coord, 0) + 1
            }
        } else if (startY == endY) {
            for (x in min(startX, endX)..max(startX, endX)) {
                val coord = x to startY;
                world[coord] = world.getOrDefault(coord, 0) + 1
            }
        } else {
            val xs = if (startX < endX) startX..endX else startX downTo endX
            val ys = if (startY < endY) startY..endY else startY downTo endY
            val coords = (xs zip ys)
            for (coord in coords) {
                world[coord] = world.getOrDefault(coord, 0) + 1
            }
        }
    }
    return world.values.count { it > 1 }
}
