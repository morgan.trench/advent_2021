package day_2

import measureMedians
import java.io.File

fun main() {
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun stepOne(): Int {
    var x = 0;
    var y = 0;
    File("data/day_2.txt").useLines { lines ->
        lines.map { it.split(" ") }.map { it[0] to it[1].toInt() }.forEach { (direction, amount) ->
            when (direction) {
                "forward" -> x += amount
                "down" -> y += amount
                "up" -> y -= amount
            }
        }
    }
    return x * y
}

fun stepTwo(): Int {
    var x = 0;
    var y = 0;
    var aim = 0;
    File("data/day_2.txt").useLines { lines ->
        lines.map { it.split(" ") }.map { it[0] to it[1].toInt() }.forEach { (direction, amount) ->
            when (direction) {
                "forward" -> {
                    x += amount
                    y += aim * amount
                };
                "down" -> aim += amount
                "up" -> aim -= amount
            }
        }
    }
    return x * y
}