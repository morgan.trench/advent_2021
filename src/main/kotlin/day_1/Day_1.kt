package day_1

import measureMedians
import java.io.File

fun main(){
    println("Answer 1: ${stepOne()}")
    println("Answer 2: ${stepTwo()}")
    println("median durations were ${measureMedians(listOf(::stepOne, ::stepTwo)).joinToString(" and ")} respectively")
}

fun stepOne(): Int = File("data/day_1.txt").useLines { lines -> lines.mapNotNull { it.toIntOrNull() }.countIncreases() }

fun stepTwo(): Int = File("data/day_1.txt").useLines { lines -> lines.mapNotNull { it.toIntOrNull() }.windowed(3).map { it.sum() }.countIncreases() }

private fun Sequence<Int>.countIncreases() = this.windowed(2).count { it[0] < it[1] }